##    (11th) 12th - 16th July 2022

### Data management and processing in archaeology

Archaeology is a discipline that produces more and more data in many forms, whether from fieldwork or research. By its nature, archaeology works on incomplete and contriving data, but the development of digital tools now make it possible to consider applying our questions to larger quantities of variables and information, particularly through LOD.  
The MOSAICdata Summer School aims to bring together PhD candidates and researchers to discuss the management of this data and the tools available to process it. Therefore, it will be a matter of developing existing techniques for collecting archaeological data, curating them, aligning them, and implementing them. Finally, it will also be an opportunity to discuss good practices when using digital data.

### Organisers

Loup Bernard, Oliver Nakoinz, Lizzie Scholtus, Franziska Engelbogen

### Contact

mosaicdata@tutanota.com 