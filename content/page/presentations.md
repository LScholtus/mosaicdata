---
title: Blocks
subtitle: Presentations
comments: false
---

## Block 0: Introduction to R

[Part 1](https://filedn.eu/lcqNcPK63RVBA8px6W2nJXB/praes/MOSAICdata/MOSAICdata_block0_1b.html#(1))  

[Part 2](https://filedn.eu/lcqNcPK63RVBA8px6W2nJXB/praes/MOSAICdata/MOSAICdata_block0_2b.html#(1))  

[Part 3](https://filedn.eu/lcqNcPK63RVBA8px6W2nJXB/praes/MOSAICdata/MOSAICdata_block0_3b.html)

[exercice correction](/MOSAICdata_block0_3b_solution.html)

## Block 1: Regular Expressions

[O. Marlet presentation](/Block_1_Marlet.pdf)

[R. Witz presentation](https://misha.gitpages.huma-num.fr/demo/regex/presentation.html)  

[Exercices](https://misha.gitpages.huma-num.fr/demo/regex/)  

[gitlab link](https://gitlab.huma-num.fr/misha)

## Block 2: Conceptual Database Modelling

[O. Nakoinz presentation](https://filedn.eu/lcqNcPK63RVBA8px6W2nJXB/praes/MOSAICdata/MOSAICdata_block2.html#(1))

## Block 3: Handling and manipulating spatial data

[G. Günther presentation 1](/spatial_data_basics.html)  

[G. Günther presentation 2](/spatial_data_in_r.html)

## Block 4: Quantitative and reproducible analysis

[L. Bernard presentation](/Howto.pdf)  

[L. Scholtus presentation](/block4.html)  

[Exercices L: Scholtus](exercice4.html)
