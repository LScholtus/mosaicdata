---
title: Information
subtitle: Venue
comments: false
---

## Maison Interuniversitaire des Sciences de l'Homme - Alsace

The summerschool will be held at the Maison Interuniversitaire des Sciences de l'Homme -Alsace (MISHA) in Strasbourg :  

5 Allée du Général Rouvillois  
67083 Strasbourg  
France

[misha website](https://www.misha.fr/contact-et-acces)

