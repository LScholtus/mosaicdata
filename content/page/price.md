---
title: Information
subtitle: Price
comments: false
---


Lunch and accommodation in a youth hostel will be provided free of charge for the participants. Participants will have to pay for their own travel cost.
