---
title: About MOSAICdata
subtitle: International Research School
comments: false
---

## Data management and processing in archaeology

Archaeology is a discipline that produces more and more data in many forms, whether from fieldwork or research. By its nature, archaeology works on incomplete and contriving data, but the development of digital tools now make it possible to consider applying our questions to larger quantities of variables and information, particularly through LOD.  

The MOSAICdata Summer School aims to bring together PhD candidates and researchers to discuss the management of this data and the tools available to process it. Therefore, it will be a matter of developing existing techniques for collecting archaeological data, curating them, aligning them, and implementing them. Finally, it will also be an opportunity to discuss good practices when using digital data.  

This summer school will last six days, during which several data management and processing methods of different fields will be illustrated and implemented using the Rstudio software. Following theoretical background and presentations of existing workflows, the afternoon workshops are dedicated to practical examples provided by our lecturers or the participants.



## Organisation

### Institutes

- Maison Interuniversitaire des Sciences de l'Homme Alsace (MISHA)
- Université de Strasbourg
- Université Franco-Allemande / Deutsch-Französische Hochschule
- Institute of Pre- and Protohistoric Archaeology
- Johanna Mestorf Academy - Kiel University

### Organisation Committee

- Lizzie Scholtus, Dr., Strasbourg University, UMR 7044 Archimède, France
- Loup Bernard, Dr., Lecturer, Strasbourg University, UMR 7044 Archimède, France
- Oliver Nakoinz, Prof. Dr., Lecturer, Institute of Pre- and Protohistoric Archaeology, Kiel University, Germany
- Franziska EngelBogen, Msc, Scientific coordinator of CRC 1266, Kiel University, Germany

### Guest Lecturers

- Gerrit Günther, Msc, Departement of Gepography, Kiel University, Germany
- Xavier Rodier, HDR, Research engineer CNRS, UMR Université-CNRS 7324 - CITERES, France
- Néhémie Strupler, Dr., Archaeological Unit, French Institute for Anatolian Studies IFEA, Turkey
- Régis Witz, Research officer in Digital Humanities, MISHA Strasbourg, France
