---
title: Application
subtitle: Application form to MOSAICdata
comments: false
---

Applications are open to researchers and young researchers (doctoral and post-doctoral), as well as to all archaeology professionals. 
Although an interest in data processing and analysis is expected from participants, a thorough knowledge of the methodology or software is not required.

To apply to this summerschool please fill in [this application form](/application_form.pdf) and send it to : 
[mosaicdata@tutanota.com](mosaicdata@tutanota.com)


[Download the application form](/application_form.pdf)

Please send this form before February 28th