---
title: Information
subtitle: Accomodation
comments: false
---

## Night of 10 July

The participants arriving on the 10 July will be accomodated in the CIARUS youth hostel:

CIARUS  
7 Rue Finkmatt  
67000 Strasbourg  
[CIARUS website](https://www.ciarus.com/fr)  


*   To go there from the train station you can take the bus 10 outside of the station direction "Brant Université". Go out on the third stop "Place de Pierre".
*   From the airport, you can take a train that will bring you to the train station and then take the bus.  
*   If you come by car, follow the direction "Avenue des Vosges".  


*   Bedding and breakfast is included in this hostel. The sleeping arrangement will be in dormitories with a bathroom in the room.  


*   To go to the university and the MISHA building from this hostel you can go by foot (30 min walk) or take the bus C or E at the stop "République" direction "Neuhof Reuss" or "Baggersee" and go out at the stop "Observatoire". These bus are replacement of the tramways that will be under construction during our week.

## Nights from 11 to 17 July

For the rest of the week all participants will be accomodated in the youth hostel:

Auberge de Jeunesse des 2 rives  
9 rue des Cavaliers  
67000 Strasbourg  
[Hostel website](https://www.hifrance.org/auberges-de-jeunesse/strasbourg-2-rives/)

*   To go there from the train station you can take the bus 2 outside of the station direction "Jardin des deux rives" and go out on the last stop (Jardin des deux rives). From there you have 650 m to walk. It is also possible to go to the underground tram station and take the tram D direction Kehl Rathaus and go out at the stop "Port du Rhin". From there, either you walk 17 min or you can take the bus 2.  
*   From the airport, you can take a train that will bring you to the train station and then follow the same instruction.  
*   If you come by car, follow the direction RN4/A4 and "Pont de l'Europe", the hostel is at 800 m from the bridge.  

*   Bedding and breakfast is included in this hostel. The sleeping arrangement will be in rooms with 3 to 4 beds.    

*   To go to the university and the MISHA building from this hostel you can take the bus 2 direction "Elmerfrost" and go out at the stop "Observatoire".

## Bus and tramways in Strasbourg

The city of Strasbourg is really well doted in public transportation. A bus or tramway ticket cost 2€ on bord (only possible for the bus), 1,80€ on the first buy, 1,70€ to recharge a ticket. All tickets are rechargeable so don't through them away after the first use.  

[CTS website](https://www.cts-strasbourg.eu/fr/se-deplacer/temps-reel/)  
[transports map](https://www.cts-strasbourg.eu/export/sites/default/pdf/CTS_plan_detaille_20800e_01-2021_V10.pdf)

