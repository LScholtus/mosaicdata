---
title: Program of MOSAICdata
subtitle: International Research School
comments: false
---

This summerschool will last six days during which several data management and processing methods, in different fields, will be illustrated and implemented using the Rstudio software.

![Program](/program.png)