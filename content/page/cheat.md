---
title: Cheat Sheets
subtitle: How to ... ?
comments: false
---

The following **cheat sheets** are quite usefull when working with R. There are much more available online...

[Importing Data](/data-import.pdf)

[Data Transformation](/data-transformation.pdf)

[Purrr](/purrr.pdf)

[R Studio](/rstudio-ide.pdf)

[Strings](/strings.pdf)

[Handling Lists](/04%20Basic%20-%20Lists%20Cheat%20Sheet.pdf)

[Data Visualization](/data-visualization-2.1.pdf)

[ggplot](/ggplot2-cheatsheet-2.0.pdf)